/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
private:
    inline unsigned long long getNumberFromListNode(ListNode *node){
        unsigned long long number = 0;
        unsigned long long multiply = 1;
        
        while(node != NULL){
            number += node->val * multiply;
            multiply *= 10;
            node = node->next;
        }
        
        return number;
    }
    
    inline ListNode* createNumberToListNode(unsigned long long number){
        ListNode* node = NULL;
        ListNode* head = NULL;
        ListNode* last = NULL;
        
        while(head==NULL || number != 0){
            node = new ListNode(number % 10);
            number /= 10;
            
            if(head == NULL){
                head = node;
                last = node;
            } else {
                last->next = node;
                last = node;
            }
        }
        
        return head;
    }

public:
    inline ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        unsigned long long number = getNumberFromListNode(l1) + getNumberFromListNode(l2);
        ListNode* node = createNumberToListNode(number);
        
        return node;
    }
};
